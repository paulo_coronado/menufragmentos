package com.coronado.bottombar;



import static android.content.ContentValues.TAG;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;

public class MainActivity extends AppCompatActivity {

    FrameLayout contenedorFragmentos;

    BottomNavigationView menuInferior;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        menuInferior= findViewById(R.id.menuInferior);
        //Listener para capturar los eventos al seleccionar un item del menú
        //Requiere como argumento un objeto que implmente la interface NavigationBarView
        menuInferior.setOnItemSelectedListener(
                new NavigationBarView.OnItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        //Aquí se programa lo que sucede cuando se hace click en algún item
                        //Obtener el id del item seleccionado

                        int id=item.getItemId();
                        Log.d(TAG, String.valueOf(id));

                        switch (id){

                            case R.id.iCalc:
                                //TO DO cargar el fragmento de la calculadora
                                /*
                                FragmentManager miAdministrador;
                                FragmentTransaction miTransaccion;
                                miAdministrador=getSupportFragmentManager();
                                miTransaccion=miAdministrador.beginTransaction();
                                miTransaccion.replace(R.id.contenedorFragmentos, new FragmentoCalculadora());
                                miTransaccion.commit();
                                */
                                getSupportFragmentManager().beginTransaction().replace(R.id.contenedorFragmentos, new FragmentoCalculadora()).commit();
                                break;

                            case R.id.iTeams:
                                //TO DO cargar el fragmento de los equipos
                                break;

                            case R.id.iMap:
                                //TO DO cargar el fragmento de los mapas
                                break;
                        }


                        return false;
                    }
                }


        );


    }
}